﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MagicDoorEventSender : MonoBehaviour
{
    public MagicDoorPlayerUserInfo m_observed;
    public OnBooleanChange m_isUsefullToPlayer;

    [Header("Debug")]
    [SerializeField] bool m_isUsefullState;
    [SerializeField] bool m_isInRoom;
    [SerializeField] bool m_isView;
    public void Update()
    {
        bool previous = m_isUsefullState;
        m_isUsefullState = m_observed.IsPlayerInRoomOrWatchingIt();
        m_isInRoom = m_observed.IsPlayerInRoom();
        m_isView = m_observed.IsPlayerWatchRoom();
        if (previous != m_isUsefullState) {
            m_isUsefullToPlayer.Invoke(m_isUsefullState);
        }
    }


    [System.Serializable]
    public class  OnBooleanChange : UnityEvent<bool>{

    }
}
