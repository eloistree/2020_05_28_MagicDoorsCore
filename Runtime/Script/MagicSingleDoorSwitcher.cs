﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicSingleDoorSwitcher : MonoBehaviour
{

    public MagicRoomLoader m_magicRoomLoader;
    public Transform m_whereToSpawn;
    public bool m_affectScale = true;
    public int m_maxListCount = 25;
    [Header("Debug")]
    public GameObject m_createdInstance;
    public ShufflableRoomsQueue m_shuffleQueue;
    public List<GameObject> m_previousList= new List<GameObject>();
    public int m_previousCount;

    void Start()
    {
        m_shuffleQueue = m_magicRoomLoader.GetRoomsInShuffleQueue(true);
        SpawnNext();
        
    }
    public void SpawnPrevious()
    {
        if (m_previousList.Count <= 0) return;
        m_previousCount++;

        int index = m_previousList.Count-1 - m_previousCount;
        if (index < 0) return;
        SpawnPrefab(m_previousList[index]);

    }
    public  void SpawnNext()
    {
        GameObject prefab = m_shuffleQueue.GetCurrent();
        m_shuffleQueue.Next();

        if (prefab == null) 
            return;

        SpawnPrefab(prefab);
        m_previousList.Add(prefab);
        m_previousCount = 0;

    }

    public void SpawnPrefab(GameObject prefab)
    {
        if (m_createdInstance != null)
            Destroy(m_createdInstance);
        GameObject instance = GameObject.Instantiate(prefab, m_whereToSpawn);
        instance.transform.localPosition = Vector3.zero;
        instance.transform.localRotation = Quaternion.identity;
        if (m_affectScale)
            instance.transform.localScale = Vector3.one;
        m_createdInstance = instance;
        while (m_previousList.Count > m_maxListCount) {
            m_previousList.RemoveAt(0);
        }
    }

}
