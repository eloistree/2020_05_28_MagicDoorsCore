﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

public class MagicDoubleDoorSwitcher : MonoBehaviour
{
    public MagicRoomLoader m_roomLoader;
    public RoomInScene m_roomOne = new RoomInScene();
    public RoomInScene m_roomTwo = new RoomInScene();
    public ShufflableRoomsQueue m_roomsQueue = new ShufflableRoomsQueue();
    [System.Serializable]
    public class RoomInScene {
      
        public MagicDoorPlayerUserInfo m_roofInfo;
        [Header("Debug")]
        public bool m_hadBeenVisited;
        public bool m_hadBeenViewed;
        public bool m_needToBeRendered;
        public GameObject m_currentPrefabUsed;
        public GameObject GetPrefabUsed()
        {
            return m_currentPrefabUsed;
        }
        public void SetCurrentPrefabUsed(GameObject prefab)
        {
            m_currentPrefabUsed = prefab;
        }
    }
    public void SwitchRoomA()
    {
        RequestChangeOnRoom(m_roomOne, true);
    }
    public void SwitchRoomB()
    {
        RequestChangeOnRoom(m_roomTwo, true);
    }

 

    public void Awake()
    {
        m_roomsQueue = m_roomLoader.GetRoomsInShuffleQueue(true);
        RequestChangeOnRoom(m_roomOne, true);
        RequestChangeOnRoom(m_roomTwo, true);
    }

    public void Update()
    {
        CheckIfNeedToBeSwitch(m_roomOne);
        CheckIfNeedToBeSwitch(m_roomTwo);

    }

    private void CheckIfNeedToBeSwitch(RoomInScene room)
    {
        bool needToBeRenderedOne = room.m_roofInfo.IsPlayerInRoomOrWatchingIt();
        if (needToBeRenderedOne != room.m_needToBeRendered)
        {
            if (!needToBeRenderedOne)
                RequestChangeOnRoom(room, true);
        }
        room.m_needToBeRendered = needToBeRenderedOne;
    }

    public RoomInScene[] GetRooms() { return new RoomInScene[] { m_roomOne, m_roomTwo }; }

    public void RequestChangeOnRoom( RoomInScene room, bool affectScale)
    {
        if (m_roomsQueue == null && m_roomsQueue.GetCount() <= 0)
            return;
        GameObject choosed=null;//= m_roomsQueue.GetCurrent();
        for (int i = 0; i < 10; i++)
        {
            choosed = m_roomsQueue.GetCurrent();
            m_roomsQueue.Next();
            if (choosed != m_roomTwo.GetPrefabUsed() && choosed != m_roomOne.GetPrefabUsed())
                break;
        }
        RequestChangeOnRoom(choosed, room, affectScale);

    }
    public void RequestChangeOnRoom(GameObject prefab, RoomInScene room, bool affectScale) {
        room.SetCurrentPrefabUsed(prefab);
        RemoveContent(room);
        GameObject instance = GameObject.Instantiate(prefab);
        AddContent(room,instance.transform, affectScale);

    }

    private void RemoveContent(RoomInScene room)
    {
        Transform content = room.m_roofInfo.GetContentRoot();
        List<Transform> toDelete= new List<Transform>();
        for (int i = 0; i < content.childCount; i++)
        {
            toDelete.Add(content.GetChild(i));
        }
        for (int i = 0; i < toDelete.Count; i++)
        {
            Destroy(toDelete[i].gameObject);
        }


    }

    private void AddContent(RoomInScene room, Transform instance, bool affectScale)
    {
        instance.transform.parent = room.m_roofInfo.GetContentRoot();
        instance.transform.localPosition = Vector3.zero;
        instance.transform.localRotation = Quaternion.identity;
        if(affectScale)
        instance.transform.localScale = Vector3.one;
    }
}
