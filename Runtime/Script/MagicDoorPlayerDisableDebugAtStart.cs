﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicDoorPlayerDisableDebugAtStart : MonoBehaviour
{
    public MagicDoorPlayerUserInfo m_roomLinked;

    void Awake()
    {
        if(m_roomLinked)
        m_roomLinked.UseDebugView(false);        
    }

}
