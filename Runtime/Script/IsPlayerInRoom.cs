﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsPlayerInRoom : MonoBehaviour
{
    public Transform[] m_roomBoxsZone;

    public bool IsPlayerInTheRoom()
    {
        bool isInRoom = false;
        if (PlayerViewInfo.InScene == null) return false;
        Vector3 playerPosition = PlayerViewInfo.InScene.GetPosition();

        for (int i = 0; i < m_roomBoxsZone.Length; i++)
        {
            if (IsPointInBox(m_roomBoxsZone[i], playerPosition))
            {
                return true;
            }
        }
        return isInRoom;
    }
    public bool IsPointInBox(Transform box, Vector3 worldPosition)
    {
        Vector3 localPosition;// box.InverseTransformPoint(worldPosition);
        GetLocalPointOf(box, worldPosition, out localPosition);
        ////Debug.Log("v:" + box.localScale + " - " + box.lossyScale);
        ////Debug.Log("v:" + localPosition);
        return Mathf.Abs(localPosition.x) < box.lossyScale.x / 2f &&
            Mathf.Abs(localPosition.y) < box.lossyScale.y / 2f &&
            Mathf.Abs(localPosition.z) < box.lossyScale.z / 2f;
    }
    public void GetLocalPointOf(Transform t, Vector3 world, out Vector3 local)
    {
        //http://answers.unity3d.com/questions/532297/rotate-a-vector-around-a-certain-point.html?_ga=2.85695771.343770237.1590504547-2089392457.1586257174
        world -= t.position;
        local =RotatePointAroundPivot(world, Vector3.zero, Quaternion.Inverse(t.rotation));
    }
    public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
    {
        return RotatePointAroundPivot(point, pivot, Quaternion.Euler(angles));
    }
    public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Quaternion rotation)
    {
        return rotation * (point - pivot) + pivot;
    }
}
