﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicDoorPlayerUserInfo : MonoBehaviour
{
    [SerializeField] IsObservedGroup m_isObserverInfo;
    [SerializeField] IsPlayerInRoom m_isPlayerInRoom;

    public void UseDebugView(bool displayDebugView)
    {
        if (m_debugView)
         m_debugView.gameObject.SetActive(displayDebugView);
    }

    [SerializeField] bool m_useDebugView;
    [SerializeField] Transform m_debugView;
    [SerializeField] Transform m_content;

    public bool IsPlayerInRoomOrWatchingIt() {
        return IsPlayerWatchRoom() || IsPlayerInRoom();
    }
    public bool IsPlayerWatchRoom()
    {
        return m_isObserverInfo.ArePointsInView();
    }
    public bool IsPlayerInRoom()
    {
      return   m_isPlayerInRoom.IsPlayerInTheRoom();
    }

    public Transform GetContentRoot() {
        return m_content;
    }

    private void OnValidate()
    {
        if(m_debugView != null)
         m_debugView.gameObject.SetActive(m_useDebugView);
    }
}
