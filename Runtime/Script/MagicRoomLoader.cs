﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MagicRoomLoader : MonoBehaviour
{

    [SerializeField] GameObject[] m_directLinkToPrefabs= new GameObject[0];
    [SerializeField] string[] m_folderTLoadPrefabsOf = new string[] { 
        "MagicRoomsDemo",
        "MagicDoorPrototype",
        "MagicDoorRelease",
        "MagicDoorShowable"
    };

    [SerializeField] GameObject[] m_resourcesRoomPrefabs = new GameObject[0];
    bool m_hasLoadedRoomOnce = false;

    public List<GameObject> GetRoomsLoaded(bool forceReload) {
        if(!m_hasLoadedRoomOnce || (m_hasLoadedRoomOnce && forceReload))
            ReloadRoomsFromResources();
        List<GameObject> result = new List<GameObject>();
        result.AddRange(m_directLinkToPrefabs);
        result.AddRange(m_resourcesRoomPrefabs);
        return result;
    }

    public ShufflableRoomsQueue GetRoomsInShuffleQueue(bool forceReload) {
        return new ShufflableRoomsQueue(GetRoomsLoaded(forceReload));
    }


    public void ReloadRoomsFromResources()
    {
        m_hasLoadedRoomOnce = true;
        List<GameObject> prefabsFound = new List<GameObject>();
        for (int i = 0; i < m_folderTLoadPrefabsOf.Length; i++)
        {
            GameObject[] prefabs = Resources.LoadAll<GameObject>(m_folderTLoadPrefabsOf[i]);
            if (prefabs != null) { 
                prefabsFound.AddRange(prefabs);
            }
        }
        m_resourcesRoomPrefabs = prefabsFound.ToArray();
    }
}


#region ROOM QUEUE
public class ShufflableRoomsQueue
{
    [SerializeField] int m_queueIndex = 0;
    [SerializeField] List<GameObject> m_roomQueue = new List<GameObject>();
  
    private static System.Random rng = new System.Random();
    public ShufflableRoomsQueue()
    {
    }
    public ShufflableRoomsQueue(IEnumerable<GameObject> roomPrefabs)
    {
        m_roomQueue = roomPrefabs.ToList();
        Shuffle<GameObject>(m_roomQueue);
    }

    internal int GetCount()
    {
        return m_roomQueue.Count;
    }

    public void ShuffleRoomQueue() { 
        Shuffle<GameObject>(m_roomQueue); }
    public GameObject GetCurrent() {

        if (m_roomQueue.Count <= 0) 
            return null;
        return m_roomQueue[m_queueIndex]; }
    public void Next()
    {
        if (m_roomQueue.Count <= 0) return;
        m_queueIndex++;
        if (m_queueIndex >= m_roomQueue.Count)
        {
            m_queueIndex = 0;
            Shuffle<GameObject>(m_roomQueue);
        }

    }

    public static void Shuffle<T>(List<T> list)
    {
        if (list.Count <= 0) return;
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}
#endregion